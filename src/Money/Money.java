package Money;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;

/**
 * Представляет денежную сумму.
 */
public class Money {

    private String _currency;
    private BigDecimal _amount;
    private static Map<Entry<String, String>, BigDecimal> _exchangeRates =
            new HashMap<Entry<String, String>, BigDecimal>();

    /**
     * Конструирует денежную сумму.
     * <p/>
     * Пример:
     * <code>
     * Money money = new Money("USD", 100);
     * </code>
     *
     * @param currency Код валюты в формате ISO 4217.
     * @param amount   Количество денег.
     * @throws IllegalArgumentException Пользователь передал отрицательное
     *                                  количество денег.
     */
    public Money(String currency, BigDecimal amount)
            throws IllegalArgumentException {
        _amount = amount;
        _currency = currency;
    }

    /**
     * Вернуть 10 рублей.
     *
     * @return 10 рублей.
     */
    public static Money tenRoubles() {
        return new Money("RUS", new BigDecimal(10));
    }

    /**
     * Вернуть 50 рублей.
     *
     * @return 50 рублей.
     */
    public static Money fiftyRoubles() {
        return new Money("RUS", new BigDecimal(50));
    }

    /**
     * Вернуть 100 рублей.
     *
     * @return 100 рублей.
     */
    public static Money oneHundredRoubles() {
        return new Money("RUS", new BigDecimal(100));
    }

    /**
     * Вернуть 500 рублей.
     *
     * @return 500 рублей.
     */
    public static Money fiveHundredRoubles() {
        return new Money("RUS", new BigDecimal(500));
    }

    /**
     * Вернуть 1000 рублей.
     *
     * @return 1000 рублей.
     */
    public static Money oneThousandRoubles() {
        return new Money("RUS", new BigDecimal(1000));
    }

    /**
     * Вернуть 5000 рублей.
     *
     * @return 5000 рублей.
     */
    public static Money fiveThousandRoubles() {
        return new Money("RUS", new BigDecimal(5000));
    }

    /**
     * Конвертирует текущую сумму денег в сумму денег в валюте to.
     *
     * @param to Валюта новой сковертированной суммы денег.
     * @return Сумма денег в валюте to.
     * @throws IllegalArgumentException Курс текущей валюты к валюте to не
     *                                  задан.
     */
    public Money convert(String to) throws IllegalArgumentException {
        BigDecimal rate = getExchangeRate(_currency, to);
        BigDecimal multiplyResult = _amount.multiply(rate)
                .round(new MathContext(2, RoundingMode.HALF_UP))
                .setScale(0, BigDecimal.ROUND_HALF_UP);
        return new Money(to, multiplyResult);
    }

    /**
     * Складывает текущую сумму денег с суммой денег money.
     *
     * @param money Сумма денег для сложения.
     * @throws IllegalArgumentException Курс текущей валюты к валюте суммы
     *                                  денег money не определен.
     */
    public Money add(Money money) throws IllegalArgumentException {
        Money result;

        if (_currency.equals(money._currency)) {
            BigDecimal newAmount = _amount.add(money.getAmount());
            result = new Money(_currency, newAmount);
        } else {
            BigDecimal rate = getExchangeRate(money.getCurrency(), _currency);
            BigDecimal convertedAmount = money.getAmount().multiply(rate)
                    .round(new MathContext(1))
                    .setScale(0, BigDecimal.ROUND_HALF_UP);
            BigDecimal sum = convertedAmount.add(_amount);
            result = new Money(_currency, sum);
        }

        return result;
    }

    /**
     * Вычитает из текущей суммы денег сумму денег money.
     *
     * @param money Сумма денег для вычитания.
     * @throws IllegalArgumentException Курс текущей валюты к валюте суммы
     *                                  денег money не определен.
     */
    public Money subtract(Money money) throws IllegalArgumentException {
        return add(new Money(money.getCurrency(), money.getAmount().negate()));
    }

    /**
     * Сравнить денежные суммы.
     *
     * @param money Сумма для сравнения.
     * @return Результат сравнения.
     */
    public int compare(Money money) {
        int result;
        if (_currency.equals(money.getCurrency())) {
            result = _amount.compareTo(money.getAmount());
        } else {
            Money converted = money.convert(_currency);
            result = _amount.compareTo(converted.getAmount());
        }

        return result;
    }

    /**
     * Склонировать денежную сумму.
     *
     * @return Клон денежной суммы.
     */
    @Override
    public Money clone() {
        return new Money(_currency, _amount);
    }

    /**
     * Вернуть текстовое представление денежной суммы.
     *
     * @return Текстовое представление денежной суммы.
     */
    @Override
    public String toString() {
        return _amount + " " + _currency;
    }

    /**
     * Сравнить две денежные суммы.
     *
     * @param object Объект для сравнения.
     * @return Результат сравнения.
     */
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (!(object instanceof Money)) {
            return false;
        }

        Money money = (Money) object;
        return compare(money) == 0;
    }

    /**
     * Задать курс одной валюты к другой.
     * <p/>
     * Пример.
     * <code>
     * Money.setExchangeRate("USD", "RUS", new BigDecimal(67));
     * </code>
     *
     * @param first  Валюта, курс которой будет задаваться.
     * @param second Валюта, к которой будет задаваться курс.
     * @param rate   Курс первой валюты по отношению к другой.
     * @throws IllegalArgumentException Пользователь передает две одинаковые
     *                                  валюты с курсом, не равным 1.
     */
    public static void setExchangeRate(String first, String second,
                                       BigDecimal rate)
            throws IllegalArgumentException {

        if (first.compareTo(second) == 0 &&
                rate.compareTo(new BigDecimal(1)) != 0) {
            throw new IllegalArgumentException("Нельзя задать курс валюты для" +
                    " этой же валюты, который при этом не равен 1");
        }

        SimpleEntry<String, String> entry = new SimpleEntry<String, String>
                (first, second);
        _exchangeRates.put(entry, rate);
    }

    /**
     * Получить курс одной валюты к другой.
     * <p/>
     * Пример.
     * <code>
     * Money.setExchangeRate("USD", "RUS", new BigDecimal(67));
     * Money.getExchangeRate("USD", "RUS"); // BigDecimal(67);
     * Money.getExchangeRate("RUS", "USD"); // BigDecimal(1 / 67);
     * </code>
     *
     * @param first  Валюта, курс которой будет получен.
     * @param second Валюта, к которой будет получен курс.
     * @return Курс первой валюты ко второй.
     * @throws IllegalArgumentException Курс первой валюты ко второй не
     *                                  определен.
     */
    public static BigDecimal getExchangeRate(String first, String second)
            throws IllegalArgumentException {
        BigDecimal result = _exchangeRates.get(new SimpleEntry<String, String>
                (first, second));

        if (result == null) {
            result = _exchangeRates.get(new SimpleEntry<String, String>
                    (second, first));

            if (result != null) {
                return new BigDecimal(1)
                        .divide(result, 200, RoundingMode.HALF_UP);
            } else {
                throw new IllegalArgumentException("Курс валют по отношению " +
                        "друг к другу не определены");
            }
        }

        return result;
    }

    /**
     * Возвращает название текущей валюты.
     *
     * @return Название текущей валюты.
     */
    public String getCurrency() {
        return _currency;
    }

    /**
     * Возвращает количество текущих денег.
     *
     * @return Количество текущих денег.
     */
    public BigDecimal getAmount() {
        return _amount;
    }

    /**
     * Возвращает курсы валют.
     *
     * @return Курсы валют.
     */
    public static Map<Entry<String, String>, BigDecimal> getExchangeRates() {
        return _exchangeRates;
    }

    /**
     * Установить курсы валют.
     *
     * @param exchangeRates Курсы валют.
     */
    public static void setExchangeRates(Map<Entry<String, String>, BigDecimal>
                                                exchangeRates) {
        _exchangeRates = exchangeRates;
    }
}
