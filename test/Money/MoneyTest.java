package Money;

import org.junit.Test;
import org.junit.After;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MoneyTest {

    @After
    public void after() throws Exception {
        Money.getExchangeRates().clear();
    }

    @Test
    public void testDefaultConstructor() throws Exception {
        Money money = new Money("USD", new BigDecimal(100));
        assertEquals("USD", money.getCurrency());
        assertEquals(new BigDecimal(100), money.getAmount());
    }

    @Test
    public void testConvert() throws Exception {
        Money.setExchangeRate("USD", "RUB", new BigDecimal(67));
        Money usd = new Money("USD", new BigDecimal(100));
        Money rub = usd.convert("RUB");
        assertEquals("RUB", rub.getCurrency());
        assertEquals(new BigDecimal(6700), rub.getAmount());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertWithUndefinedExchangeRate()
            throws Exception {
        Money usd = new Money("USD", new BigDecimal(100));
        Money rub = usd.convert("RUB");
    }

    @Test
    public void testAdd() throws Exception {
        Money money1 = new Money("USD", new BigDecimal(100));
        Money money2 = new Money("USD", new BigDecimal(100));
        Money actual = money1.add(money2);
        assertEquals("USD", actual.getCurrency());
        assertEquals(new BigDecimal(200), actual.getAmount());
    }

    @Test
    public void testAddDifferentCurrencies() throws Exception {
        Money.setExchangeRate("USD", "RUS", new BigDecimal(67));
        Money money1 = new Money("USD", new BigDecimal(100));
        Money money2 = new Money("RUS", new BigDecimal(6700));
        Money actual = money1.add(money2);
        assertEquals("USD", actual.getCurrency());
        assertEquals(new BigDecimal(200), actual.getAmount());
    }

    @Test
    public void testSubtract() throws Exception {
        Money money1 = new Money("USD", new BigDecimal(100));
        Money money2 = new Money("USD", new BigDecimal(100));
        Money actual = money1.subtract(money2);
        assertEquals(new BigDecimal(0), actual.getAmount());
    }

    @Test
    public void testSubtractResultIsNegative() throws Exception {
        Money money1 = new Money("USD", new BigDecimal(100));
        Money money2 = new Money("USD", new BigDecimal(200));
        Money actual = money1.subtract(money2);
        assertEquals(new BigDecimal(-100), actual.getAmount());
    }

    @Test
    public void testSubtractDifferentCurrencies() throws Exception {
        Money.setExchangeRate("USD", "RUS", new BigDecimal(67));
        Money money1 = new Money("USD", new BigDecimal(100));
        Money money2 = new Money("RUS", new BigDecimal(6700));
        Money actual = money1.subtract(money2);
        assertEquals("USD", actual.getCurrency());
        assertEquals(new BigDecimal(0), actual.getAmount());
    }

    @Test
    public void testClone() throws Exception {
        Money money = new Money("USD", new BigDecimal(100));
        Money clone = money.clone();
        assertEquals(money.getCurrency(), clone.getCurrency());
        assertEquals(money.getAmount(), clone.getAmount());
        assertTrue(money != clone);
    }

    @Test
    public void testToString() throws Exception {
        Money money = new Money("USD", new BigDecimal(100));
        assertEquals("100 USD", money.toString());
    }

    @Test
    public void testEquals() throws Exception {
        Money money1 = new Money("USD", new BigDecimal(100));
        Money money2 = new Money("USD", new BigDecimal(100));
        assertTrue(money1.equals(money2));
    }

    @Test
    public void testNotEquals() throws Exception {
        Money.setExchangeRate("USD", "RUS", new BigDecimal(67));
        Money money3 = new Money("USD", new BigDecimal(100));
        Money money4 = new Money("RUS", new BigDecimal(100));
        assertFalse(money3.equals(money4));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetExchangeRateWithInvalidParams()
            throws Exception {
        Money.setExchangeRate("RUS", "RUS", new BigDecimal(2));
    }

    @Test
    public void testSetExchangeRateWithTheSameCurrencies()
            throws Exception {
        Money.setExchangeRate("RUS", "RUS", new BigDecimal(1));
        assertEquals(new BigDecimal(1), Money.getExchangeRate("RUS", "RUS"));
    }

    @Test
    public void testGetAndSetExchangeRate() throws Exception {
        Money.setExchangeRate("USD", "RUS", new BigDecimal(67));
        BigDecimal actual = Money.getExchangeRate("USD", "RUS");
        assertEquals(new BigDecimal(67), actual);
    }

    @Test
    public void testGetExchangeRateOfSecondCurrencyToFirst() throws Exception {
        Money.setExchangeRate("USD", "RUS", new BigDecimal(67));
        BigDecimal actual = Money.getExchangeRate("RUS", "USD");
        assertEquals(new BigDecimal(1).divide(new BigDecimal(67), 200,
                RoundingMode.HALF_UP), actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetExchangeRateOfNotExistingCurrencies() throws Exception {
        Money.setExchangeRate("USD", "RUS", new BigDecimal(67));
        BigDecimal actual = Money.getExchangeRate("EUR", "RUS");
        assertEquals(null, actual);
    }

    @Test
    public void testCompareMoneyWithTheSameCurrencies() throws Exception {
        Money money1 = new Money("USD", new BigDecimal(200));
        Money money2 = new Money("USD", new BigDecimal(200));
        Money money3 = new Money("USD", new BigDecimal(300));
        Money money4 = new Money("USD", new BigDecimal(100));
        assertEquals(0, money1.compare(money2));
        assertEquals(-1, money4.compare(money1));
        assertEquals(1, money3.compare(money1));
    }

    @Test
    public void testCompareMoneyWithDifferentCurrencies() throws Exception {
        Money.setExchangeRate("USD", "RUS", new BigDecimal(67));
        Money money1 = new Money("USD", new BigDecimal(100));
        Money money2 = new Money("RUS", new BigDecimal(6700));
        Money money3 = new Money("RUS", new BigDecimal(100));
        Money money4 = new Money("RUS", new BigDecimal(10000));
        assertEquals(0, money1.compare(money2));
        assertEquals(-1, money1.compare(money4));
        assertEquals(1, money1.compare(money3));
        assertEquals(1, money4.compare(money1));
    }

    @Test
    public void testGetTenRoubles() {
        Money money1 = new Money("RUS", new BigDecimal(10));
        Money money2 = Money.tenRoubles();
        assertTrue(money1.equals(money2));
    }
}
